package cm;

import java.io.*;

public class WeatherParser {
    ResultInformation ri = new ResultInformation();

    public void doParsing(){
        File f = new File("weatherData.csv");
        try {

            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);

            for (int i = 0; i < 10; i++) {
                br.readLine();
            }
            String str = br.readLine();
            while (str != null){
                ri.addString(str.split(","));
                str = br.readLine();
            }
            br.close();
            fr.close();

            FileWriter resFile = new FileWriter(new File("Res.txt"));
            BufferedWriter bw = new BufferedWriter(resFile);
            bw.write(ri.toString());
            bw.close();
            resFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
