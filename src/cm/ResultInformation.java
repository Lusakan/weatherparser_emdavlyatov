package cm;

import java.util.Arrays;

public class ResultInformation {
    double averageWindTemperature;
    double averageHumidity;
    double averageWindSpeed;
    String maxTemperatureTimestamp;
    double maxTemperature;
    double lowestHumidity;
    double highestWindSpeed;
    int[] windDirections = new int[4];
    static int infoCounter = 0;


    @Override
    public String toString() {

        return "Average wind temperature: " + ((double)((int)(averageWindTemperature*100)))/100 + "\n" +
                "Average humidity: " + (double)((int)(averageHumidity*100))/100 + "\n" +
                "Average wind speed: " + ((double)((int)(averageWindSpeed)*100))/100 + "\n" +
                "Day and hour of maximal temperature: " + goodDataView() + "\n" +
                "Lowest humidity: " + lowestHumidity + "\n" +
                "Highest wind speed: " + ((double)((int)(highestWindSpeed)))/100 + "\n" +
                "Most popular wind direction: " + getMostPopularWindDirection() + "\n" +
                Arrays.toString(windDirections);
    }

    private String goodDataView() {
        String res = "";
        String[] str = maxTemperatureTimestamp.split("T");
        res += str[0].substring(0, 4) + "-" + str[0].substring(4, 6) + "-" + str[0].substring(6,8);
        res += " " + str[1].substring(0,2) + ":" + str[1].substring(2, 4);
        return res;
    }

    private String getMostPopularWindDirection() {
        int maxValue = 0;
        int maxValueIndex = -1;
        for (int i = 0; i < 4; i++){
            if(windDirections[i] > maxValue){
                maxValue = windDirections[i];
                maxValueIndex = i;
            }
        }
        String res = "";
        if(maxValueIndex == 0) res = "North";
        if(maxValueIndex == 1) res = "East";
        if(maxValueIndex == 2) res = "South";
        if(maxValueIndex == 3) res = "West";
        return res;
    }

    public void addString(String[] stringWithStatistic){
        if(infoCounter == 0){
            initializeStatistic(stringWithStatistic);
            infoCounter++;
        }else{
            changeAverageInfo(stringWithStatistic);
            setMaxTemperature(stringWithStatistic);
            lowestHumidity = Math.min(lowestHumidity, Double.parseDouble(stringWithStatistic[2]));
            highestWindSpeed = Math.max(highestWindSpeed, Double.parseDouble(stringWithStatistic[3]));
            setWindDirection(stringWithStatistic[4]);
        }
    }

    private void setMaxTemperature(String[] stringWithStatistic) {
        double temp = Double.parseDouble(stringWithStatistic[1]);
        if(temp > maxTemperature){
            maxTemperature = temp;
            maxTemperatureTimestamp = stringWithStatistic[0];
        }
    }

    private void changeAverageInfo(String[] stringWithStatistic) {
        averageWindTemperature =
                (averageWindTemperature*infoCounter + Double.parseDouble(stringWithStatistic[1])) /(infoCounter+1);
        averageHumidity =
                (averageHumidity*infoCounter + Double.parseDouble(stringWithStatistic[2])) /(infoCounter+1);
        averageWindSpeed =
                (averageWindSpeed*infoCounter + Double.parseDouble(stringWithStatistic[3])) /(infoCounter+1);
        infoCounter++;
    }

    private void initializeStatistic(String[] stringWithStatistic){
        averageWindTemperature = Double.parseDouble(stringWithStatistic[1]);
        averageHumidity = Double.parseDouble(stringWithStatistic[2]);
        averageWindSpeed = Double.parseDouble(stringWithStatistic[3]);

        maxTemperature = averageWindTemperature;
        maxTemperatureTimestamp = stringWithStatistic[0];
        lowestHumidity = averageHumidity;
        highestWindSpeed = averageWindSpeed;
        setWindDirection(stringWithStatistic[4]);
    }

    private void setWindDirection(String s) {
        double windDirection = Double.parseDouble(s);
        if(windDirection > 180){
            if(windDirection > 270){
                if(windDirection > 315) {windDirections[0]++;}
                else windDirections[3]++;
            }else{
                if(windDirection > 225){
                    windDirections[3]++;
                }else windDirections[2]++;
            }
        }else{
            if(windDirection > 90){
                if(windDirection > 135){
                    windDirections[2]++;
                }else{
                    windDirections[1]++;
                }
            }else{
                if(windDirection > 45){
                    windDirections[1]++;
                }else{
                    windDirections[0]++;
                }
            }
        }
    }
}
